var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var switches = require('../node_modules/vol/switches');
var fs = require('fs');
var path = require('path');
var hbs = require('hbs');

hbs.registerHelper("select", function(value, options) {
    return options.fn(this)
        .split('\n')
        .map(function(v) {
            var t = 'value="' + value + '"';
            return ! RegExp(t).test(v) ? v : v.replace(t, t + ' selected="selected"')
        })
        .join('\n')
});

/* GET home page. */
router.get('/', function (req, res, next) {

    res.render('index', {
        title: 'volatility-web',
        files: dirTree('PLACE_MEM_DUMPS_HERE').children,
        plugins: switches.plugins(),
        profiles: switches.profiles()});
});

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.post('/', function(req, res) {

    try {
        var file = '--filename=' + req.body.memoryLocation;
        var profile = req.body.profile;
        var plugin = req.body.plugin;
        var out = "--output=json";

        var python = require('child_process').spawn(
            'python',
            ["-u",
                "C:\\Users\\Simon\\volatility\\vol.py",
                plugin,
                file,
                profile,
                out
            ]
        );

        var output = "";
        python.stdout.on('data', function (data) {
            var tmp = " " + data;
            if (tmp.indexOf('***') == -1) {
                output += data;
            }else{
                console.log(tmp);
            }
        });

        python.stderr.on('data', function (data) {
            console.log("" + data);
        });

        python.on('close', function (code) {
            console.log(output);

            return res.render('index', {
                title: 'volatility-web',
                files: dirTree('PLACE_MEM_DUMPS_HERE').children,
                lstMem: req.body.memoryLocation,
                plugins: switches.plugins(),
                lastPlugin: req.body.plugin,
                profiles: switches.profiles(),
                lastProfile: req.body.profile,
                rowTitle: JSON.parse(output)['columns'],
                result: JSON.parse(output)['rows'],
                startTable: '$(\'#resultsTable\').dataTable();',
                fileSelected: true,
                file: req.body.memoryLocation
            })
        });
    } catch(err){
        res.statusCode(500,err);
    }

});

function dirTree(filename) {
    var stats = fs.lstatSync(filename),
        info = {
            path: filename,
            name: path.basename(filename)
        };

    if (stats.isDirectory()) {
        info.type = "folder";
        info.children = fs.readdirSync(filename).map(function(child) {
            return dirTree(filename + '\\' + child);
        });
    } else {
        // Assuming it's a file. In real life it could be a symlink or
        // something else!
        info.type = "file";
    }

    return info;
}

module.exports = router;


